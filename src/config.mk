# Edit this file with your preferences, made on OpenBSD but should compile in any POSIX OS

LIBS = -lncursesw -lm
CFLAGS = -D_DEFAULT_SOURCE -D_BSD_SOURCE -D_POSIX_C_SOURCE=200809L -pedantic -Wall

CC = clang
