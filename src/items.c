/* Headers */
#include <curses.h>
#include <math.h>
#include <stdlib.h>
#include "game.h"
#include "items.h"
#include "level.h"

/* Function Definitions */
void	itemDraw(int y, int x, int type);
int	itemEffect(int y, int x);
void	itemGen(int floor);

/* Program */
/*
 * This program will manage the items drawn on top of the level.
 * Also contains some of the effects of the items in the game.
 */

/*
 * The itemDraw(int, int, int) function implements the colours of
 * the program, the symbols, and adding the items to the global
 * level array
 */
void
itemDraw(int y, int x, int type) {
	char item;

	if (type < 1000) {
	    attron(COLOR_PAIR(ITEM1COL));
	    item = '&';
	    *(*(levelG + y) + x) = item;
	} else if (type < 5000) {
	    attron(COLOR_PAIR(ITEM2COL));
	    item = 'V';
	    *(*(levelG + y) + x) = item;
	} else {
	    attron(COLOR_PAIR(ITEM3COL));
	    item = 'T';
	    *(*(levelG + y) + x) = item;
	}

	mvaddch(y, x, item);
	attroff(COLOR_PAIR(ITEM1COL));
	attroff(COLOR_PAIR(ITEM2COL));
	attroff(COLOR_PAIR(ITEM3COL));
	return;
}

/*
 * This function manages the actual effects of the items in game.
 * Simply will check the global array and call a corresponding
 * function from game.c
 */
int
itemEffect(int y, int x) {
	switch ((*(*(levelG + y) + x))) {
	    case 'V' :
	        health(20, false);
	        return 1;
	        break;
	    case '&' :
	        gas(-100, false);
	        return 2;
	        break;
	    case 'T' :
	        weapon(20, false);
	        return 3;
	        break;
	    case 'F' :
	        mvprintw(0,0,"Going up a floor");
	        inFloor = false;
	        upFloor = true;
	        return 0;
	        break;
	    default :
	        return 0;
	        break;
	}
}

/*
 * This function will generate random items in the level
 * using the seed array in game.c. It will check for collision
 * for each item as it places them using the collide function
 * in level.c. Just like the levelGen function this should be
 * 100% reversible, maybe adding storage of used items if
 * the player is to be able to descend floors.
 */
void
itemGen(int floor) {
	long int seed;
	int num, count, type, tmp, y, x, adjY, adjX;

	seed = floorSeed[floor];

	num = seed / 100000000;
	count = num;

	if (count < 4)
	    count = 4;
	if (count > 12)
	    count = 12;

	for (int i = 0; i < count; i ++) {
	    tmp = pow(seed - num * 100000000, (double)(1.00 + (double)((i + 1) / 400.00)));
	    /*if (tmp < 0) {
	        count --;
	    }*/

	    x = tmp / 1000000;

	    tmp = tmp - x * 1000000;
	    y = tmp / 10000;

	    tmp = tmp - y * 10000;
	    type = tmp;

	    /*for (int j = 0; j < 100; j ++) {
	        if (x[i] == x[j] && y[i] == y[j])
	            ident = true;
	    }*/

	    while (x > 99) {
	        x = x - 99;
	    }
	    while (y > 99) {
	        y = y - 99;
	    }

	    adjY = ((double)y / 100) * levelHt;
	    adjX = ((double)x / 100) * levelWid;

	    if (collides(adjY, adjX) == true) {
	        adjY ++;
	        adjX ++;
	    }

	    itemDraw(adjY, adjX, type);
	
	}
	return;
}



