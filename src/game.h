/* Function Prototypes */
__BEGIN_DECLS
void	drawGUI(int);
void	drawWEP();
void	game();
void	gameOver(bool, int);
double	gas(double, bool);
double	gasStep(int);
double	health(double, bool);
void	initGUI();
int	weapon(int, bool);
__END_DECLS

#define GAMEOVERCOL 124
#define PLAYCOL 69 /*nice*/

/* Shared global vars */
int gameHt;
int gameWid;
bool inFloor, upFloor;
