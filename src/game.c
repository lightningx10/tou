/* Headers */
#include <curses.h>
#include <stdlib.h>
#include "enemies.h"
#include "game.h"
#include "level.h"
#include "items.h"

/* Function Declarations */
void	drawGUI(int floor);
void	drawWEP();
void	game();
void	gameOver(bool lost, int floor);
double	gas(double change, bool full);
double	gasStep(int floor);
double 	health(double change, bool full);
void	initGUI();
int 	weapon(int change, bool full);

/* Global Variables */
double	 gasPercentage;
WINDOW	*GUI;
double	 playerHealth;
WINDOW	*WEP;
int	 weaponHealth;

char gameOverA[][51] ={	" _____                        _____                ",
			"|  __ \\                      |  _  |               ",
			"| |  \\/ __ _ _ __ ___   ___  | | | |_   _____ _ __ ",
			"| | __ / _` | '_ ` _ \\ / _ \\ | | | \\ \\ / / _ \\ '__|",
			"| |_\\ \\ (_| | | | | | |  __/ \\ \\_/ /\\ V /  __/ |   ",
			" \\____/\\__,_|_| |_| |_|\\___|  \\___/  \\_/ \\___|_|   "
};

/* Program: */
/* This file contains the bulk of the game, and is essentially a game engine, it
 * calls all other functions from the other files to itself.
 */

/*
 * This function draws the GUI that appears on the side of the players' screen,
 * needs to be simple as it will be updated every frame. (This is the health &
 * gas one in particular)
 */
void
drawGUI(int floor) {
	/* Function Variables */
	int healthX, gasX, healthBarX, gasBarX, healthValueX, gasValueX, healthBarL;
	int healthY, gasY, healthBarY, gasBarY, healthValueY, gasValueY, gasBarL;
	char *healthTitle;
	char *gasTitle;

	healthX = 3;
	healthY = 1;
	gasX = getmaxx(GUI) - 7;
	gasY = 1;
	healthTitle = "Health:";
	gasTitle = "Gas:";

	healthValueY = getmaxy(GUI) - 4;
	healthValueX = healthX + 2;

	healthBarY = healthValueY - 2;
	healthBarX = healthValueX;

	gasValueY = healthValueY;
	gasValueX = gasX;

	gasBarY = healthValueY - 2;
	gasBarX = gasValueX;

	healthBarL = (playerHealth / 100) * (getmaxy(GUI) - 8);
	gasBarL = (gasPercentage / 100) * (getmaxy(GUI) - 8);

	werase(GUI);
	box(GUI, 0, 0);
	mvwprintw(GUI, healthY, healthX, healthTitle);
	mvwprintw(GUI, gasY, gasX, gasTitle);

	mvwprintw(GUI, healthValueY, healthValueX, "%.0f%%", playerHealth);
	mvwprintw(GUI, gasValueY, gasValueX, "%.1f%%", gasPercentage);

	mvwprintw(GUI, getmaxy(GUI) - 2, 6, "Floor: %d", floor);

	for (int i = 0; i < healthBarL; i ++) {
	    for (int j = 0; j < 2; j ++) {
	        wattron(GUI, COLOR_PAIR(ITEM2COL));
	        wmove(GUI, healthBarY - i, healthBarX + j);
	        waddch(GUI, '#');
	        wattroff(GUI, COLOR_PAIR(ITEM2COL));
	    }
	}

	for (int i = 0; i < gasBarL; i ++) {
	    for (int j = 0; j < 2; j ++) {
	        wmove(GUI, gasBarY - i, gasBarX + j);
	        waddch(GUI, '#');
	    }
	}
	wrefresh(GUI);
	return;
}

/*
 * This function draws the other bit of GUI on the side of the screen, the one
 * with weapon stats. Ironically even though it doesn't need to be as light as
 * the drawGUI() function, it is similar. Only runs when weapon stats are
 * updated.
 */
void
drawWEP() {
	/* Function Variables */
	int titleY, titleX, percentageY, percentageX, barY, barX, barL;
	char *title;

	titleY = 1;
	titleX = 7;
	title = "Weapon:";

	werase(WEP);

	box(WEP, 0, 0);
	mvwprintw(WEP, titleY, titleX, title);

	percentageY = getmaxy(WEP) - 2;
	percentageX = (getmaxx(WEP) / 2) - 2;

	barY = percentageY - 2;
	barX = percentageX + 1;
	barL = (weaponHealth / 100) * (getmaxy(GUI) - 6);

	mvwprintw(WEP, percentageY, percentageX, "%d%%", weaponHealth);

	for (int i = 0; i < barL; i ++) {
	    for (int j = 0; j < 2; j++) {
	        wmove(WEP, barY - i, barX + j);
	        waddch(WEP, '#');
	    }
	}
	wrefresh(WEP);
	return;
}


/*
 * This will be the main game function, the game should
 * NOT exit this function for too long. There will be many
 * smaller functions, but this should bring them all together
 * and act as a backend/engine for the game itself.
 */
void
game()
{
	/* Function Variables */
	int floor, midX, midY, playerX, playerY, playerXOld, playerYOld;
	bool quit, lost;

	/* Initialising Variables */
	/* Setting Game Width */
	gameWid = getmaxx(stdscr);
	gameHt = getmaxy(stdscr);
	levelWid = gameWid - 21;
	levelHt = gameHt;

	playerHealth = 100.00;
	weaponHealth = 100;
	gasPercentage = 0.00;

	if (has_colors() == true) {
	    init_pair(PLAYCOL, COLOR_CYAN, COLOR_BLACK);
	    init_pair(ITEM1COL, COLOR_GREEN, COLOR_BLACK);
	    init_pair(ITEM2COL, COLOR_RED, COLOR_BLACK);
	    init_pair(ITEM3COL, COLOR_YELLOW, COLOR_BLACK);
	    init_pair(ENEMYCOL, COLOR_RED, COLOR_BLACK);
	    init_pair(WALLCOL, COLOR_WHITE, COLOR_BLACK);
	    init_pair(GAMEOVERCOL, COLOR_RED, COLOR_BLACK);
	    init_pair(KEYCOL, COLOR_BLACK, COLOR_YELLOW);
	    if (can_change_color() == true) {
	        init_color(ITEM2COL, 999, 0, 670);
	        init_pair(ITEM2COL, ITEM2COL, COLOR_BLACK);
	        init_color(WALLCOL, 700, 700, 700);
	        init_pair(WALLCOL, WALLCOL, COLOR_BLACK);
	    }
	}

	/* Setting middle position of game */
	midX = gameWid / 2;
	midY = gameHt / 2;

	/* Setting up floors */
	floor = 0;
	quit = false;

	/* Drawing & generation starts here: */
	clear();
	/* Generating seeds for each floor */
	for (int i = 0; i <= 100; i ++) {
	    floorSeed[i] = seedGen();
	}
	initGUI();

	upFloor = false;

	while (quit == false) {
	    if (upFloor == true)
	        floor ++;
	    /* Draw the desired floor */
	    levelDraw(floor);
	    itemGen(floor);
	    enemyGen(floor);
	    drawGUI(floor);
	    drawWEP();
	    inFloor = true;

	    /* Check for player collision at initial position */
	    if (collides(midY, midX) == false) {
	        playerX = midX;
	        playerY = midY;
	    } else {
	        playerX = midX + 1;
	        playerY = midY + 1;
	    }

	    /* Game ``engine'': */
	    while (inFloor == true) {
	        /* Draw & manage the player's coords */
	        attron(COLOR_PAIR(PLAYCOL));
	        mvaddch(playerY, playerX, '@');
	        attroff(COLOR_PAIR(PLAYCOL));
	        playerYOld = playerY;
	        playerXOld = playerX;

	        /* Player input */
	        switch (getch()) {
	            /* Up */
	            case 'k' :
	            case 'w' :
	            case 259 :
	                if (collides(playerY - 1, playerX) == false) {
	                    playerY --;
	                    enemyStep(playerY, playerX, floor);
	                    gas(gasStep(floor), false);
	                }
	                break;

	            /* Down */
	            case 'j' :
	            case 's' :
	            case 258 :
	                if (collides(playerY + 1, playerX) == false) {
	                    playerY ++;
	                    enemyStep(playerY, playerX, floor);
	                    gas(gasStep(floor), false);
	                }
	                break;

	            /* Left */
	            case 'h' :
	            case 'a' :
	            case 260 :
	                if (collides(playerY, playerX - 1) == false) {
	                    playerX --;
	                    enemyStep(playerY, playerX, floor);
	                    gas(gasStep(floor), false);
	                }
	                break;

	            /* Right */
	            case 'l' :
	            case 'd' :
	            case 261 :
	                if (collides(playerY, playerX + 1) == false) {
	                    playerX ++;
	                    enemyStep(playerY, playerX, floor);
	                    gas(gasStep(floor), false);
	                }
	                break;

	            /* Quit the game */
	            case 'q' :
	                quit = true;
	                inFloor = false;
	                lost = false;
	                break;

	            case 'p' :
	                enemyStep(playerY, playerX, floor);
	                gas(gasStep(floor), false);
	                break;

	            default :
	                break;
	        }
	        /* Remove old player character */
	        mvaddch(playerYOld, playerXOld, ' ');

	        enemyCollides(playerY, playerX);
	        drawGUI(floor);
	        if (health(0, false) == 0.00) {
	            quit = true;
	            inFloor = false;
	            lost = true;
	        }
	    }
	}
	if (lost) {
	    gameOver(true, floor);
	} else {
	    gameOver(false, floor);
	}
	return;
}

/*
 * This is the function that will display the screen after the user either
 * quits, or dies in game. If the player dies, the text will be in red with
 * the floor number that the player survived until
 */
void
gameOver(bool lost, int floor) {
	clear();
	int y, x;

	y = getmaxy(stdscr) / 2 - 3;
	x = getmaxx(stdscr) / 2 - 25;

	if (lost) {
	    attron(COLOR_PAIR(GAMEOVERCOL));
	    for (int i = 0; i < 6; i ++) {
	        for (int j = 0; j < 51; j ++) {
	            mvaddch(y + i, x + j, gameOverA[i][j]);
	        }
	    }
	    attroff(COLOR_PAIR(GAMEOVERCOL));
	    mvprintw(y + 8, x + 12, "You survived until floor %d", floor);
	} else {
	    for (int i = 0; i < 6; i ++) {
	        for (int j = 0; j < 51; j ++) {
	            mvaddch(y + i, x + j, gameOverA[i][j]);
	        }
	    }
	}
	
	getch();

	napms(1000);
	return;
}

/*
 * This function manages the global gas variable, rather than making other
 * funtions each manage it separately. A fairly simple function that acts
 * as a range limiter for the gas variable.
 */
double
gas(double change, bool full) {
	gasPercentage += change;
	if (full == true || gasPercentage > 100.00) {
	    gasPercentage = 100.00;
	}
	if (gasPercentage < 0.00) {
	    gasPercentage = 0.00;
	}

	health(-gasPercentage * 0.005, false);

	return gasPercentage;
}

/*
 * This function determines the rate at which the gas is increasing based on the
 * value for the current floor. Obviously higher floors result in faster gas.
 */
double
gasStep(int floor){
	/* Function Variables */
	double levelMod;

	if (floor < 30) {
	    levelMod = 0.75 * ((double)floor / 100.00);
	} else {
	    levelMod = 0.75 * (30.00 / 100.00) + 0.5 * ((double)floor / 100.00);
	}
	
	return levelMod;
}

/*
 * This function much like the gas() function is just to limit the modification
 * of the playerHealth variable to one spot.
 */
double
health(double change, bool full) {
	playerHealth += change;
	if (full == true || playerHealth > 100) {
	    playerHealth = 100;
	}
	if (playerHealth < 0) {
	    playerHealth = 0;
	}
	return playerHealth;
}

/*
 * This function is run on game initialization, and initializes the GUI.
 * Made separate so that these redundant actions do not have to occur
 * every frame.
 */
void
initGUI() {
	/* Function Variables */
	int ht, wid, y, x;

	ht = getmaxy(stdscr) / 3;
	wid = getmaxx(stdscr) - (levelWid + 1);
	y = 0;
	x = levelWid + 1;

	GUI = newwin(ht, wid, y, x);
	box(GUI, 0, 0);
	wrefresh(GUI);
	refresh();

	y = ht + 1;

	WEP = newwin(ht, wid, y, x);
	box(WEP, 0, 0);
	wrefresh(WEP);

	return;
}

/*
 * This function much like the gas() and health() functions manages the
 * ``Health'' of the nondescript weapon that the player uses.
 */
int
weapon(int change, bool full) {
	weaponHealth += change;
	if (full == true || weaponHealth > 100) {
	    weaponHealth = 100;
	}
	if (weaponHealth < 0) {
	    weaponHealth = 0;
	}
	drawWEP();
	return weaponHealth;
}
