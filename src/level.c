/* Headers */
#include <curses.h>
#include <math.h>
#include <stdlib.h>
#include "level.h"
#include "items.h"

/* Function Definitions */
bool		collides(int y, int x);
void		levelDraw(int floor);
void		levelGen(char level[levelHt][levelWid], int floor);
void		roomGen(char level[levelHt][levelWid], int y, int x, int ht, int wid);
long int	seedGen();

/* Program: */
/*
 * This file is for managing the level that is used by the game, it contains the
 * level & part of the item collision logic.
 */

/*
 * This function will act as the collision detection from the game, it should
 * be as fast as possible, because it will need to run every time the player makes
 * a movement. Thus it is currently a very basic logic gate.
 */
bool
collides(int y, int x)
{
	/* Grabs the Global level array's contents and checks if they're blank */
	if (*(*(levelG + y) + x) == ' ' || *(*(levelG + y) + x) == 'V' || *(*(levelG + y) + x) == '&' || *(*(levelG + y) + x) == 'T' || *(*(levelG + y) + x) == 'F') {
	    itemEffect(y,x);
	    return false;
	} else
	    return true;
}

/*
 * This function should simply generate and draw the level, it will likely call
 * another function for the generation itself, but it shouldn't need to run very
 * often or for very long.
 */
void
levelDraw(int floor)
{
	/* Function Variables */
	char level[levelHt][levelWid];
	int keyY, keyX;

	/* Blanking out the level */
	for (int i = 0; i < levelHt; i ++) {
	    for (int j = 0; j < levelWid; j ++) {
	        *(*(level + i) + j) = ' ';
	    }
	}

	/* Adding the partition between the game and the GUI */
	for (int i = 0; i < levelHt; i ++) {
	    move(i,levelWid);
	    addch(ACS_VLINE);
	}

	/* Level generation */
	levelGen(level, floor);

	keyY = ((arc4random_uniform(100) / 100.00) * levelHt);
	keyX = ((arc4random_uniform(100) / 100.00) * levelWid);
	if (*(*(level + keyY) + keyX) == '=' || *(*(level + keyY) + keyX) == '#') {
	    keyY ++;
	    keyX ++;
	}
	*(*(level + keyY) + keyX) = 'F';

	/* Mirroring the level array into a global array & draws the final level */
	for (int i = 0; i < levelHt; i ++) {
	    for (int j = 0; j < levelWid; j ++) {
	        move(i,j);

	        if (*(*(level + i) + j) == '=' || *(*(level + i) + j) == '#')
	            attron(COLOR_PAIR(WALLCOL));
	        else if (*(*(level + i) + j) == 'F')
	            attron(COLOR_PAIR(KEYCOL));
	        addch(*(*(level + i) + j));
	        attroff(COLOR_PAIR(WALLCOL));
	        attroff(COLOR_PAIR(KEYCOL));

	        *(*(levelG + i) + j) = *(*(level + i) + j);
	    }
	}

	/* Debug Printing */
	/*
	printw("s: %d, f: %d", floorSeed[floor], floor);
	*/
	return;
}

/*
 * This function will be the one that actually generates the random rooms and
 * such. It should do the same thing every time a given seed is given to it.
 * Seeds are given through the floor counter, the function will search the
 * floorSeed[] array for the seed of the floor.
 */
void
levelGen(char level[levelHt][levelWid], int floor)
{
	/* Function Variables */
	long int seed;
	int count, num, retry, roomDRW, tmp;
	int y[100];
	int x[100];
	int ht[100];
	int wid[100];
	int adjY, adjX, adjHt, adjWid;
	bool collide;

	/* Initialising Variables */
	/* Room error variables, roomDRW counts drawn rooms, collide is set when
	 * overlap is detected, retry counts the number of retries total.
	 */
	roomDRW = 0;
	collide = false;
	retry = 0;

	/* Generation: */
	/* Setting the floorseed */
	seed = floorSeed[floor];

	/* Getting the number of rooms */
	num = seed / 100000000;
	count = num;
	if (count < 4)
	    count = 4;

	/* Room generation: (i refers to the room number)*/
	for (int i = 0; i < count; i ++) {
	    /* Changes seed for each room & retry */
	    tmp = pow(seed - num * 100000000, (double)(1.00 + (double)((i + retry)/1600.00)));
	    if (tmp < 0 || retry > 100000) {
	        count --;
	    }

	    /* Getting room x */
	    x[i] = tmp / 1000000;

	    /* Getting room y */
	    tmp = tmp - x[i] * 1000000;
	    y[i] = tmp / 10000;

	    /* Getting room wid */
	    tmp = tmp - y[i] * 10000;
	    wid[i] = tmp / 100;

	    /* Getting room ht */
	    tmp = tmp - wid[i] * 100;
	    ht[i] = tmp;

	    /* Lowering widths that go out of the screen */
 	    while (x[i] + wid[i] > 100) {
	        x[i] = x[i] / 2;
	        wid[i] = wid[i] / 2;
	    }
	    /* Lowering heights that go out of the screen */
	    while (y[i] + ht[i] > 100) {
	        y[i] = y[i] / 2;
	        ht[i] = ht[i] / 2;
	    }
	    /* Lowering x values that are too big */
	    while (x[i] >= 100) {
	        x[i] = x[i] - 100;
	    }
	    /* Lowering y values that are too big */
	    while (y[i] >= 100) {
	        y[i] = y[i] - 100;
	    }
	    /* Setting minimum width and height */
	    if (wid[i] < 10)
	        wid[i] = 10;
	    if (ht[i] < 10)
	        ht[i] = 10;

	    /* Box collision detection */
	    if (i > 0) {
	        for (int j = 1; j < i + 1; j ++) {
	            /* Check if box xy within a previous box */
	            if (x[i] >= x[i - j] && y[i] >= y[i - j] && x[i] <= (x[i - j] + wid[i - j]) && y[i] <= (y[i - j] + ht[i - j])) {
	                collide = true;
	            } /*
	               * Check if box bottom right corner lands in another box
	               */ else if ((x[i] + wid[i] >= x[i - j] && x[i] + wid[i] <= x[i - j] + wid[i - j]) && (y[i] + ht[i] >= y[i - j] && y[i] + ht[i] <= y[i - j] + ht[i - j])) {
	                collide = true;
	            } /*
	               * Check if box bottom left corner lands in another box
	               */ else if ((x[i] >= x[i - j] && x[i] <= x[i - j] + wid[i - j]) && (y[i] + ht[i] >= y[i - j] && y[i] + ht[i] <= y[i - j] + ht[i - j])) {
	                collide = true;
	            } /*
	               * Check if box left side overlaps another box
	               */ else if (x[i] >= x[i - j] && y[i] <= y[i - j] && y[i] + ht[i] >= y[i - j]) {
	                collide = true;
	            } /*
	               * Check if box right side overlaps another box
	               */ else if ((x[i] <= x[i - j] && x[i] + wid[i] >= x[i - j]) && (y[i - j] && y[i] + ht[i] >= y[i - j]) ) {
	                collide = true;
	            }
	        }
	        /* If there are any collisions, the loop will repeat */
	        if (collide == true) {
	            retry ++;
	            i --;
	            collide = false;
	        }
	    }
	}

	/* Getting y & x values from the percentages */
	for (int i = 0; i < count; i ++) {
	    adjY = ((double)y[i] / 100) * levelHt;
	    adjX = ((double)x[i] / 100) * levelWid;
	    adjHt = ((double)ht[i] / 100) * levelHt;
	    adjWid = ((double)wid[i] / 100) * levelWid;

	    /* If the new y& x values are somehow out of bounds */
	    if (adjY + adjHt < levelHt && adjY > 0 && adjX + adjWid < levelWid && adjX >0) {
	        roomGen(level, adjY, adjX, adjHt, adjWid);
	        roomDRW ++;
	    }
	}
	/* If all were out of bounds, just draw a square at 10,10 */
	if (roomDRW == 0) {
	    roomGen(level, 10, 10, 10, 10);
	}
	return;
}

/*
 * This function is simply to add a room with the supplied width and height, at
 * the given x and y co-ordinates in the supplied level array.
 */
void
roomGen(char level[levelHt][levelWid], int y, int x, int ht, int wid)
{
	/* Drawing loop */
	for (int i = 0; i < ht; i ++) {
	    for (int j = 0; j < wid; j ++) {
	        /* Drawing the top & bottom of the room */
	        if (i == 0 || i == (ht - 1)) {
	            /* Drawing the entrances */
	            if ((j == wid/2 || j == wid/2 + 1 || j == wid/2 - 1) && (y + i != 0) && (y + ht + i != levelWid))
	                *(*(level + i + y) + j + x) = ' ';
	            else
	                *(*(level + i + y) + j + x) = '=';
	        }
	        /* Drawing the left and right side of the room */
	        else if (j == 0 || j == (wid - 1)) {
	            /* Drawing the entrances */
	            if ((i == ht/2) && (x + j != 0) && (x + wid + j != levelWid))
	                *(*(level + i + y) + j + x) = ' ';
	            else
	                *(*(level + i + y) + j + x) = '#';
	        } else
	            *(*(level + i + y) + j + x) = ' ';
	    }
	}
	return;
}

/*
 * This function should create as random a seed as possible, and return it as
 * a long int. The final function will likely have seeds of all the same length
 * to make game logic easier. (Optimised currently for OpenBSD's random() function.)
 */
long int
seedGen()
{
	/* OBSD old one */
	/*long int seed = random();*/

	/* new good one */
	long int seed = arc4random_uniform(999999999);
	return seed;
}
