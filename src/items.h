/* Function Prototypes */
__BEGIN_DECLS
void	itemDraw(int, int, int);
int	itemEffect(int, int);
void	itemGen(int);
__END_DECLS

/* Global Constants */
#define ITEM1COL 100
#define ITEM2COL 101
#define ITEM3COL 102
#define ITEM4COL 103
