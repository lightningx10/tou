/* Headers */
#include <curses.h>
#include <math.h>
#include <stdlib.h>
#include "enemies.h"
#include "game.h"
#include "items.h"
#include "level.h"

/* Function Definitions */
void	enemyCollides(int y, int x);
void	enemyDraw(int y, int x, int count);
void	enemyGen(int floor);
void	enemyStep(int y, int x, int floor);

/* Global Variables */
int enemyYX[100][2];
int enemyCount;

/* Program: */
/* 
 * The enemy.c file will manage enemy attributes and how they act.
 * All enemy movement and interaction should be done here.
 */

/*
 * The enemyCollides(int, int) function is somewhat misleading, as it
 * actually deals with enemies hitting the player rather than the level.
 */
void
enemyCollides(int y, int x) {
	/* Function Variables */
	int damage;
	bool found;

	damage = -20;
	found = false;


	for (int i = 0; i < enemyCount; i ++) {
	    if (enemyYX[i][0] == y && enemyYX[i][1] == x) {
	        found = true;
	        enemyCount --;
	        if (weapon(0, false) == 0.00)
	            health(damage, false);
	        else
	            weapon(damage / 2, false);
	    }
	    if (found == true) {
	        int j = i + 1;
	        *(*(enemyYX + i )) = *(*(enemyYX + j ));
	        *(*(enemyYX + i) + 1 ) = *(*(enemyYX + j) + 1 );
	    }
	}
	return;
}

void
enemyDraw(int y, int x, int count) {
	*(*(enemyYX + count)) = y;
	*(*(enemyYX + count) + 1) = x;
	attron(COLOR_PAIR(ENEMYCOL));
	mvaddch(y, x, 'X');
	attroff(COLOR_PAIR(ENEMYCOL));
	return;
}

/*
 * This function will generate the starting positions for every one of the enemies in a
 * level. Another derivative of the levelGen function, extremely similar to the itemGen
 * function except the number of enemies will increase as the floor number increases.
 */
void
enemyGen(int floor) {
	long int seed;
	int num, count, tmp, y, x, adjY, adjX;

	seed = floorSeed[floor];

	num = seed / 100000000;
	enemyCount = num;

	for (int i = 0; i < enemyCount; i ++) {
	    tmp = pow(seed - num * 100000000, (double)(1.00 + (double)(i / 600.00)));

	    x = tmp / 1000000;

	    tmp = tmp - x * 1000000;
	    y = tmp / 10000;

	    while (x > 99) {
	        x = x - 99;
	    }
	    while (y > 99) {
	        y = y - 99;
	    }
	    adjY = ((double)y / 100) * levelHt;
	    adjX = ((double)x / 100) * levelWid;

	    if (collides(adjY, adjX) == true) {
	        adjY ++;
	        adjX ++;
	    }
	    if (itemEffect(adjY, adjX) != 0) {
	        adjY ++;
	        adjX ++;
	    }

	    enemyDraw(adjY, adjX, i);
	}
	return;
}

void
enemyStep(int y, int x, int floor) {
	bool found;
	int oldY, oldX, newY, newX;
	double prob;

	for (int i = 0; i < enemyCount; i ++) {
	    oldY = newY = *(*(enemyYX + i));
	    oldX = newX = *(*(enemyYX + i) + 1);
	    mvaddch(oldY, oldX, ' ');

	    prob = arc4random_uniform(100) / 100.00;
	    if (prob < (0.3 + (floor / 200.00))) {
	        if (y < newY) {
	            if (collides(newY - 1, newX) == false)
	                newY --;
	        }
	        if (x < newX) {
	            if (collides(newY, newX - 1) == false)
	                newX --;
	        }
	        if (y > newY) {
	            if (collides(newY + 1, newX) == false)
	                newY ++;
	        }
	        if (x > newX) {
	            if (collides(newY, newX + 1) == false)
	                newX ++;
	        }
	    }

	    if (collides(newY, newX) == true) {
	        newY = oldY;
	        newX = oldX;
	    }
	    enemyDraw(newY, newX, i);
	}
	return;
	    
}
