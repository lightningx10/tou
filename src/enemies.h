/* Function Prototypes */
__BEGIN_DECLS
void	enemyCollides(int, int);
void	enemyDraw(int, int, int);
void	enemyGen(int);
void	enemyStep(int, int, int);
__END_DECLS

/* Global Constants */
#define ENEMYCOL 200
