/* File Independent Definitions */
#define WALLCOL 50
#define KEYCOL 30

/* File Independent Global Variables */
long int globalSeed;
int levelWid;
int levelHt;
char levelG[1000][1000];
long int floorSeed[100];

/* Function Prototypes */
__BEGIN_DECLS
bool		collides(int, int);
void		levelDraw(int);
void		levelGen(char[levelHt][levelWid], int);
void		roomGen(char[levelHt][levelWid], int, int, int, int);
long int	seedGen();
__END_DECLS
