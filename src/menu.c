/* Headers */
#include <curses.h>
#include <locale.h>
#include "menu.h"
#include "game.h"

/* Function Input Declarations */
void	 aboutMenu();
WINDOW	*button(int ht, int wid, int y, int x, char message[], bool sel);
void	 draw();
void 	 navigation(bool quit);
bool	 selectButton(int active);
void 	 title(int y, int x);
int	 main();

/* Global Variables */
#define BUTLENGTH 3
#define SETLENGTH 5

int xPosition, yPosition;
WINDOW *but[4];
WINDOW *setBut[5];

/* Program: */
/*
 * This is the menu part of the game, not too important, but a good menu system can
 * really help make a good impression that your program is functional. The menu will
 * appear between the game and the shell, the user should have the option to exit to
 * the shell or the main menu. The settings menu will likely not change too much other
 * than saving the game.
 */

/*
 * This function will function(bruh) as the about menu, displaying information about the
 * game, the authors and licencing, and maybe version numbering.
 */
void
aboutMenu()
{
	/* Function Variables */
	int y, x;
	WINDOW *about;
	char *aboutMessage;
	int messageLength, aboutHt, aboutWid;

	y = getmaxy(stdscr) / 2;
	x = getmaxx(stdscr) / 2;
	aboutHt = 10;
	aboutWid = 54;

	nodelay(stdscr, true);
	clear();
	move(y - (aboutHt / 2) - 1, x);
	printw("About:");
	about = newwin(aboutHt, aboutWid, y - aboutHt / 2, x - aboutWid / 2);
	refresh();
	box(about, 0, 0);
	aboutMessage = "JigokuNoTou, made by Ethan Long with the unlicense, check the UNLICENSE file for more details. Made in Cwith the ncurses library. Bugs/equiries? email me atedl@disroot.org";
	for (messageLength = 0; aboutMessage[messageLength] != '\0'; messageLength ++);
	for (int i = 0; i < aboutHt - 2; i ++) {
	   for (int j = 0; j < aboutWid - 2; j ++) {
	      if (messageLength > 0) {
	         wmove(about, i + 1, j + 1);
	         waddch(about, *(aboutMessage + i * (aboutWid - 2) + j));
	         wrefresh(about);
	         messageLength --;
	         napms(15);
	         getch();
	      }
	   }
	}
	napms(1000);
	aboutMessage = "Press any key to continue...";
	for (messageLength = 0; aboutMessage[messageLength] != '\0'; messageLength ++);
	for (int i = 0; i < messageLength; i ++) {
	   wmove(about, getmaxy(about) - 2, getmaxx(about) - 1 - messageLength + i);
	   waddch(about, *(aboutMessage + i));
	   wrefresh(about);
	   napms(15);
	   getch();
	}
	getch();
	nodelay(stdscr, false);
	getch();
	clear();
	return;
}

/*
 * Prints a menu button with the supplied width, height, x, y, and message.
 * This function also deals with whether or not the button is selected or not.
 */
WINDOW
*button(int ht, int wid, int y, int x, char message[], bool sel)
{
	/* Function Variables */
	WINDOW *button;
	int messageLength;
	int offsetX;
	int iY, iX;

	/* Creating a button with the input variables */
	button = newwin(ht, wid, y, x);
	if (sel == true) {
	    wattron(button, COLOR_PAIR(SEL_P));
	} else {
	    wattron(button, COLOR_PAIR(BUT_P));
	}
	refresh();

	/* Finding the length of the button message */
	for (messageLength = 0; message[messageLength] != '\0'; messageLength ++);
	offsetX = (messageLength / 2);

	/* Drawing the button */
	box(button, 0, 0);
	iY = getmaxy(button) / 2;
	iX = getmaxx(button) / 2;
	wmove(button, iY, iX - offsetX);
	wprintw(button, message);
	wrefresh(button);

	return button;
}

/*
 * The draw() function performs the initialisation of the menu, setting the colours and
 * setting the cursor position. This should not be running for large periods of time,
 * offload any loops to other functions.
 */
void
draw()
{
	/* Function Variables */
	bool quit;

	/* Set (y,x) to the middle of the screen */
	yPosition = getmaxy(stdscr) / 2;
	xPosition = getmaxx(stdscr) / 2;

	/* Defining Colours */
	start_color();
	init_pair(BUT_P, COLOR_WHITE, COLOR_BLACK);
	init_pair(SEL_P, COLOR_BLACK, COLOR_WHITE);

	/* Selection Routine */
	quit = false;
	navigation(quit);
	return;
}


/*
 * The navigation() function will do the brunt of the navigation, essentially acts as a
 * big while loop. The function does not actually refresh constantly, as it waits for
 * user input through getch(). Ideally should not refresh every frame, as that would be
 * stupid for a menu.
 */
void
navigation(bool quit)
{
	/*	Function Variables	*/
	int y, x, butActive;

	y = yPosition;
	x = xPosition;
	butActive = 0;
	while (!quit)
	{
	    /* Draw the title */
	    title(0, x - (TITLE_ARRAY_LENGTH / 2));
	    mvprintw(y + 5, x - 23, "Controls: Down-j, Up-k, Select-<Enter> or l, Quit-q");
	    refresh();	//refresh the screen

	    /* Refresh each button */
	    but[0] = button(5, 21, y - 10, x - 10, "Play", butActive == 0);
	    wrefresh(but[0]);
	    but[1] = button(3, 21, y - 3, x - 10, "About", butActive == 1);
	    wrefresh(but[1]);
	    but[2] = button(3, 21, y + 1, x - 10, "Quit", butActive == 2);
	    wrefresh(but[2]);

	    /* Move the curser to be on the button */
	    move(getbegy(but[butActive]), getbegx(but[butActive]));

	    /* Menu Input: */
	    switch (getch()) {
	        case 'j' :
	        case 's' :
	        case 258 :
	            /* Down */
	            if (butActive + 1 >= BUTLENGTH)
	                butActive = 0;
	            else
	                butActive ++;
	            clear();
	            break;
	        case 'k' :
	        case 'w' :
	        case 259 :
	            /* Up */
	            if (butActive - 1 < 0)
	                butActive = BUTLENGTH - 1;
	            else
	                butActive --;
	            clear();
	            break;
	        case 'l' :
	        case 'd' :
	        case 261 :
	        case 13 :
	            /* Selected */
	            quit = selectButton(butActive);
	            break;
	        case 'q' :
	            quit = true;
	            break;
	        default :
	            break;
	    }
	}
	return;
}

/*
 * This function handles the logic behind each button, if a button is selected,
 * this will decide what happens next, essentially just a case statement.
 * The boolean nature is helpful for making menus to exit out of, any button with ID 3
 * will return a true.
 */
bool
selectButton(int active)
{
	switch (active) {
	    case 0 :
	        game();
	        clear();
	        break;
	    case 1 :
	        aboutMenu();
	        break;
	    case 2 :
	        return true;
	        break;
	    default :
	        break;
	}
	return false;
}

/*
 * Prints the title
 */
void
title(int y, int x)
{
	/* Function Variables */
	int rowN, colN;

	/* Get the number of rows and columns */
	rowN = sizeof(titleA) / sizeof(titleA[0]);
	colN = TITLE_ARRAY_LENGTH;

	/* Draw the 2D title array */
	for (int i = 0; i < rowN; i ++) {
	    for (int j = 0; j < colN; j ++) {
	        mvaddch(y + i + 1, x + j + 1, titleA[i][j]);
	    }
	}
	refresh();
	return;
}

/*
 * The main() function performs all the initialisation functions, setting the screen up,
 * allowing or disallowing <Ctrl> + c exiting, and blocking the echoing of typed
 * characters, is also vital that it is returned to so that the game exits without
 * error, and so that garbage collection can take place.
 */
int
main()
{
	/* curses Setup: */
	setlocale(LC_ALL, "");
	initscr();
	cbreak();
	noecho();
	nonl();
	intrflush(stdscr, FALSE);
	keypad(stdscr, TRUE);
	curs_set(0);

	draw();

	/* Garbage Collection: */
	endwin();
	return 0;
}
