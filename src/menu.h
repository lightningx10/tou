/* Function Prototypes */
__BEGIN_DECLS
void 	 aboutMenu();
WINDOW 	*button(int, int, int, int, char[], bool);
void 	 draw();
void	 navigation(bool);
bool 	 selectButton(int);
void 	 title(int, int);
int	 main();
__END_DECLS

/* Global Defs */
#define BUT_P 1
#define SEL_P 2
#define TITLE_ARRAY_LENGTH 101

/* Title */
/*char titleA[][52] = {   "___________               __                   __   ",
 *                        "\\__    ___/___ ______   _/  |_  ____ ___  ____/  |_ ",
 *                        "  |    | /  _ \\\\____ \\  \\   __\\/ __ \\\\  \\/  /\\   __\\",
 *                        "  |    |(  <_> )  |_> >  |  | \\  ___/ >    <  |  |  ",
 *                        "  |____| \\____/|   __/   |__|  \\___  >__/\\_ \\ |__|  ",
 *                        "               |__|                \\/      \\/       " };
 */

/*
 * Following is a prototype for the new title, it looks relatively good
 * but I would like to get the one with extended ASCII working,
 * I will look into it.
 */
char titleA[][101] = {		"   ###          ###             ### ####    ##              #########          ###       ##   ##     ",
				"   ###          ###            ###  ^^^^    ###          ####   ##  ####       ###   ################",
				"#########  ###  #########     ##   ######   ## ##      ###     ###     ###   #######     ##   ##     ",
				"   ###     ########   ##    ## ##           ##        ###      ###      ###    ###          #        ",
				"   ###  ######  ###  ##         ##  ####  ########   ###       ###      ###    ###       ### ###     ",
				"   ###     ###  ###  ##       ####         ##       ###        ###     ###     ###    ###       ###  ",
				"   ### ##  ###  ###         ##  ##  ####   ###      ###       ###      ###     ###   #   #######   # ",
				"   #####   ###                 ##    __   ## ##      ###     ###      ###   mmm###^^     _______     ",
				"  ####     ###          #  #  ##    #^^# ##   ###      ########      ###   ^^           #^^^^^^^#    ",
				"#####       #############   ##      #mm# #     #####     #####      ###                 #mmmmmmm#    "};

/* Shared Global Variable Declirations */

