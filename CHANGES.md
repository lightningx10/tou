# Changes:
#### All the changes to the project will be put in here as a record.
	- Implementing a globally addressable level array involved lots of
		memory management with malloc and calloc, which I deemed to be
		too hard for such a simple thing. Collisions will instead be
		tested using the currently drawn objects.
