.AU
Ethan Long
.TL
Function report & descriptions
.AB no
.AE

.KS
.NH
Programme design
.PP
The game is obviously going to be written in C, as a mostly functional program with few 
Object-Oriented design decisions.
The program will be divided into a bunch of smaller 
.CW *.c
and 
.CW *.h
files, which in turn are compiled to
.CW *.o
files, which are then statically linked together into the game executable.
.B
FINAL LAYOUT TBD

.UL "Diagram of compilation process:"
.R
.PS
down
MK: box "make"
arrow .3 from MK.s
CC: box "cc"

move down .3 from CC.s
move left 2.5
down
MENh: box "menu.h"
move right .625 from MENh
GAMh: box "game.h"
move right .625 from GAMh
LEVh: box "level.h"
move right .625 from LEVh
ITMh: box "items.h"
move right .625 from ITMh
SCRh: box "score.h"
move right .625 from SCRh
ENMh: box "enemies.h"

arrow from CC.s to MENh.n
arrow from CC.s to GAMh.n
arrow from CC.s to LEVh.n
arrow from CC.s to ITMh.n
arrow from CC.s to SCRh.n
arrow from CC.s to ENMh.n

move down .3 from MENh.s
MENc: box "menu.c"
move right .625 from MENc
GAMc: box "game.c"
move right .625 from GAMc
LEVc: box "level.c"
move right .625 from LEVc
ITMc: box "item.c"
move right .625 from ITMc
SCRc: box "score.c"
move right .625 from SCRc
ENMc: box "enemies.c"

arrow from MENh.s to MENc.n
arrow from GAMh.s to GAMc.n
arrow from LEVh.s to LEVc.n
arrow from ITMh.s to ITMc.n
arrow from SCRh.s to SCRc.n
arrow from ENMh.s to ENMc.n

move down 1.9 from CC.s
LNK: box "link"
arrow .3
box "JigokuNoTou" wid 1

arrow from MENc.s to LNK.n
arrow from GAMc.s to LNK.n
arrow from LEVc.s to LNK.n
arrow from ITMc.s to LNK.n
arrow from SCRc.s to LNK.n
arrow from ENMc.s to LNK.n
.PE
List of files and their functions:
.TS
 _ _
|c|c|
 = =
|c|r|
 _ _
|c|r|
 _ _
|c|r|
 _ _
|c|r|
 _ _
|c|r|
 _ _
|c|r|
 _ _
|c|r|
 _ _
|c|r|
 _ _
|c|r|
 _ _
|c|r|
 _ _
|c|r|
 _ _
|c|r|
 _ _.

File:	Function:

menu.c	To draw the menu, and link to other files

menu.h	To define all the functions in use by menu.c

game.c	To bring all the game functions together, and draw them on the screen

game.h	To define all the functions in use be game.c

level.c	To generate a random level for the game

level.h	To define all the functions in use by level.c

items.c	To generate the items in a level, will know the positions of the walls and generate random items.

items.h	To define all the functions and variables in use by items.c

enemies.c	To generate and update enemy positions (Likely won't be very advanced)

enemies.h	To define all the functions in use by enemies.c

score.c	To keep track of (and maybe store the score)

score.h	To define all the functions in use by score.c

.TE
.KE

.NH 2
.CW menu.c 
functions:
.R

.P1
void aboutMenu();
.P2
This function is to draw the about menu, will clear the screen and print information about the
game, author etc. No inputs and no outputs.

.P1
WINDOW *button(int ht, int wid, int y, int x, char[] message, bool sel);
.P2
This window pointer function creates a button with the following attributes:
A position of 
.CW x , (
.CW y ) 
with width
.CW wid
and height
.CW ht .
Additionally takes an array of characters that make the displayed
.CW message ,
and the boolean 
.CW sel
to determine whether or not it should be drawn as the selected button.
All other button management goes on outside of this function, this simply gives properties
to the button.

.P1
void draw();
.P2
This is the function that handles the initialisation of colour, the default curser position and the
variable for quitting. Returns to the function where it is called from (Typically should be the
main function so that the program exits properly.

.P1
void navigation(bool quit);
.P2
This is the function that the menu will be in for the majority of the time, the
.CW quit boolean
given to it is also the function it will return true when quit.
Stays in a centre while loop for the most part, does not refresh every frame, opting
to wait for user input, as it is a menu.
Relies upon the
.CW title 
function, the
.CW *button 
function, and the
.CW selectButton
function in its typical operation.

.P1
bool selectButton(int active);
.P2
This is the function that determines what each button does when selected, takes the integer
.CW active
as the input, putting it into a case statement that determines what is run for each button.
As currently set up, an input of
.CW 0
will send the program to the game() function in 
.CW game.c .
An input of
.CW 1
will send the program to the settingsMenu() function, to set up the settings.
An input of
.CW 2
will send the program to the aboutMenu() function, to show the about page.
An input of
.CW 3
will return
.CW true ,
corresponding with the quit button, useful for exiting back out of a while loop.
Any other input will do nothing, corresponding to an undocumented button.

.P1
void settingsMenu();
.P2
Draws a menu to turn on or off high score saving, and configure audio (terminal beeping).
Should essentially act as another menu loop, with buttons to select in a similar way to the main
menu.

.P1
void title(int y, int x);
.P2
Draws the title described in
.CW menu.h 
by
the 
.CW titleA 
char
array at position
.CW x , (
.CW y ).

.NH 2
.CW game.c
functions:

.P1
void game();
.P2
The
.CW game()
function will be the main function that controls the game, should draw a frame on
every action.

.P1
void gameSpeedUp(int amount);
.P2
The
.CW gameSpeedUp()
function will take an integer and increase the game speed by that amount as a percentage of the
current speed.

.P1
void gameSpeedDown(int amount);
.P2
The
.CW gameSpeedDown()
function will take an integer and decrease the game speed by that amount as a percentage of the
current speed.

.P1
void playSpeedUp(int amount);
.P2
The
.CW playSpeedUp()
function will take an integer and increase the player's speed by that amount as a percentage of their
current speed.

.P1
void playSpeedDown(int amount);
.P2
The
.CW playSpeedDown()
function will take an integer and decrease the player's speed by that amount as a percentage of their
current speed.

.P1
void gasSpeedUp(int amount);
.P2
The
.CW gasSpeedUp()
function will take an integer and increase the speed of the gas by that amount as a percentage of its
current speed.

.P1
void gasSpeedDown(int amount);
.P2
The
.CW gasSpeedDown()
function will take an integer and decrease the speed of the gas by that amount as a percentage of its
current speed.

.P1
void gasValueUp(int amount);
.P2
The
.CW gasValueUp()
function will take an integer and increase the player's current gas % amount by that much as a percentage.

.P1
void gasValueDown(int amount);
.P2
The
.CW gasValueDown()
function will take an integer and decrease the player's current gas % amount by that much as a percentage.

.P1
void healthValueUp(int amount);
.P2
The
.CW healthValueUp()
function will take an integer and increase the player's current health percentage by the given amount.

.P1
void healthValueDown(int amount);
.P2
The
.CW healthValueDown()
function will take an integer and decrease the player's current health percentage by the given amount.

.P1
void gotoLevel(int floor, long int seed);
.P2
The
.CW gotoLevel()
function will take an integer for the floor and the seed to use, and will run all the required functions
to go to the given level.

.P1
void addEffect(int item);
.P2
The
.CW addEffect()
function will take an integer representing the item, and apply the necessary effects to the player using
the effect functions in
.CW game.c .

.NH 2
.CW level.c
functions:

.P1
int seedGen();
.P2
The
.CW seedGen()
function will generate and return a seed every time it is called.

.P1
char *level(int floor, long int seed);
.P2
The
.CW level()
function will generate the level based on the input seed.
Will return the level to wherever it is called from. Input
.CW floor
is to ensure that each level is different despite having the same global seed, allowing one to technically
descend a level (potential power up or gameplay aspect.)


.NH 2
.CW items.c
functions:

.P1
int *items(int floor, long int seed);
.P2
The 
.CW items()
function will generate and return random items that are in positions that don't clash with the wall generating
algorithm in
.CW level() .
The items will be described by preset integer values to be converted later for use with drawing and item effects.

.P1
char getItem(char *items, int y, int x);
.P2
The
.CW getItem()
function will take the input array describing the current field of 
.CW items ,
and position
.CW x , (
.CW y ),
and will ouput a character describing the item to be drawn on screen.
Should be a fast function, as it will be called on a regular basis every time a item needs to be drawn.

.NH 2
.CW score.c
functions:

.P1
int scoreMultiplier(int level);
.P2
The
.CW scoreMultiplier()
function will take the level that the player is on, and spit out the appropriate multiplier for that level.

.P1
void putHighScore(char[] name, int score);
.P2
.P1
void getHighScores();
.P2
These two functions will be responsible for the saving of high scores. The
.CW putHighScore()
function will take a name, and a score and append it to a new line of the high scores file, which will be located
in the directory that the game is runinng in.

.NH 2
.CW enemies.c
functions:

.P1
char *placeEnemies(int floor, long int seed);
.P2
This function will set the initial position for the enemies, placing them in around the level, it will be different
depending on the seed used.

.P1
void *moveEnemies(int enemySpeed, int playerY, int playerX, char *enemyPositions, char *level);
.P2
This function will move the enemies closer to the player, by using the player's (x,y) position, and very VERY basic
pathfinding (I.E. Head straight for them once seen)

.P1
boolean canSee(char *level, int enemyY, int enemyX, int playerY, int playerX);
.P2
This boolean will return true if an enemy is in eyesight of the player, essentially making the enemy able to move.

