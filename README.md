# 地獄の塔 (The tower of hell)

A game in C using the ncurses library for a Computer Science Class. Public Domain, free to use for whatever.

## Table of contents:
[Brief](#brief)

[Preliminary Design Report](#PDR)

<a name="brief"/>

## Project Brief:
### Game Description:
This game will be an ASCII/ncurses based dungeon game with randomised dungeons.
It will not play like a typical dungeon game in the sense that you will spend most of your time running away.
The player will be running away from a cloud of gas that is approaching them at an ever increasing rate.
There will be a third dimension to the dungeon, hence the name of the game, indicating that you will be
constantly elevating in altitude, however you won't be able to see what is on the plane above you, and you
can't descend.
The player will have a health bar, and there will be a fog bar that describes the concentration of the gas
that is getting into the players' blood.
On each level will be items that hold back the fog, increase player health, and decrease the gas entering the
blood (I.E. gas masks).
As you climb in altitude the fog will speed up, so players will need to be tactical in their climbing, as if
you climb up a level, the fog speed increases.
Each level starts from zero fog, to stop players from constantly climbing, the player will lose health whenever
they climb a level.
Thus, the player must be both patient, but fast in their rate of collecting.
It will be a game of skill, not particularly RPG-like except for the fact that you gain abilities as the game
gets harder.
Needs to be easy to get into, and put down quickly, no saving apart from maybe a highscore mechanic.
Each floor will be a set of random rooms, with random exits and entrances between them (preferably more
laberinth like the higher one climbs), and with random item locations.
Items should be clear and obvious, both in their design and location, so that players can be more tactical in
their choices as they enter the room (Shouldn't feel like it's all up to luck of the draw).

### Target Audience:
The target audience for this game would likely be a Unix sysadmin, or someone using a terminal a lot, who is
bored and wants something to do for a brief period of time.
The game isn't intentionally being made hard to run, to use or to understand, but it isn't likely going to be
running on a mobile phone and due to the nature of ncurses, won't be particularly visually appealing


### Game Ideas
The player will start in a room, being tied down, the illusion of a small illuminated area will be presented to
the player.
The text "Press <left> and <right> to break free!" will appear telling the player to break out of the contraints,
contextually the constraints might be represented as unicode diamonds or <> or something that attempts to make it
obvious.
(Potentially even a detailed ASCII art close up of the player to illustrate the situation.)
Breaking the constraints will alert an alarm, that will in turn start the gas coming into the room.

<a name="PDR"/>

## Preliminary Design Report:
To find the PDR, look in the PDR folder, there is a PDF of the PDR.


# BUGS:
The only current bug is that it won't compile in gcc, will find commands to
attempt to fix that on GNU systems.

