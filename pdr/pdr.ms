.de FG	\" .FG <basename>
.ie h .html - <img src="\\$1.\\$2" />
.el .BP \\$1.ps
.br
..
.AU
Ethan Long
.TL
Jigoku no Tou: A Preliminary Design Report

.AB no
.AE

.NH
User interface design
.PP
The basic user interface will consist of either functions drawn over the game, or of separate windows
(A feature of ncurses).
Since it is in ncurses, it will obviously be basic ASCII art, the main menu currently looks like this:
.FG menuScrot

The ncurses library allows for custom colour definition, and a function to detect terminal support, so
there will be more colour in the final menu.

Ideally the menu boxes will also be completely filled in.

The game will have a health and gas bar on the right hand side of the screen, the health bar going down
at a rate determined by the size of the gas bar, and being replenished with items that are acquired
by the player as they complete a floor.

Points will simply be decided by time spent alive, the longer you live for, the more points you get.
(Maybe on an exponential scale, or at least matching the difficulty increases.) Items will also net
points.

.KS
.NH 2
Menu
.PP
.PS
OL: box wid 5 ht 5
move down .1 from OL.n
TL: box "Jigoku No Tou (in ASCII ART)" wid 2.5 ht 1
move down .15 from TL.c
"The Tower of Hell"
move up .1 from OL.c
PLAY: box "Play" wid 1 ht .4

move left .2725 from OL.c
move down .1
SETT: box "Settings" wid .475 ht .2
move right .2725 from OL.c
move down .1
ABOUT: box "About" wid .475 ht .2

move down .4 from OL.c
QUIT: box "Quit" wid 1 ht .2
move down .1 from QUIT.s
"Controls: Down-j or s, Up-k or w, Select-<Enter> or l or d, Quit-q"

move down 1 from OL.ne
move right .2
PLAYEXP: box "Sends the user to the tutorial" above fill wid 2 ht .5
move down .05 from PLAYEXP.c
"screen or the game" 
move down .15
"(Should be a toggle)"
arrow from PLAYEXP.w to PLAY.e

move down .25 from PLAYEXP.s
SETTEXP: box "Sends the user to the settings menu" fill wid 2 ht .25
arrow from SETTEXP.w to SETT.e

move down .25 from SETTEXP.s
ABOUTEXP: box "Sends the user to the about menu" fill wid 2 ht .25
arrow from ABOUTEXP.w to ABOUT.e

move down .25 from ABOUTEXP.s
QUITEXP: box "Sends the user back to where they" above fill wid 2 ht .5
move down .05 from QUITEXP.c
"came from"
arrow from QUITEXP.w to QUIT.e

.PE
.KE

.KS
.NH 2
Tutorial Screen (Before the game)
.PP
.PS
OL: box wid 5 ht 2 fill
move down .1 from OL.n
"The game uses 3 possible control schemes that can be switched between on the fly"
move down .15
"The preferred scheme is the vi-style scheme, that uses the following Controls:"
move down .15
"H - Move left, J - Move down, K - Move up, L - Move right"
move down .35
"Alternatively one can use the typical ``Gamer'' scheme:"
move down .15
"A - Move left, S - Move down, W - Move up, D - Move right"
move down .35
"Alternatively if you are a pleb, you can use the arrow keys."

move down .15
"To (attack and) use items, press the space bar key."
move down .15
"The key for what each ascii character means will go here"

move down .25
"<Press any key to continue to the game>"
.PE
.KE

.KS
.NH 2
Settings Menu
.PP
.PS
OL: box wid 5 ht 5
move down 1 from OL.n
box fill "Settings:"
move down 2 from OL.n
TUT: box "Tutorials" wid 1 ht .25
move down .05 from TUT.s
SAV: box "Saving" wid 1 ht .25
move down .05 from SAV.s
VOL: box "Volume" wid 1 ht .25
move right .15 from VOL.s
move down .05
BACK: box "Back" fill wid 1.3 ht .25

move right .05 from TUT.e
TUTC: box "X" wid .25 ht .25

move right .05 from SAV.e
SAVC: box "X" wid .25 ht .25

move right .05 from VOL.e
VOLC: box "X" wid .25 ht .25

move down 1 from OL.ne
move right .2
TUTEXPL: box "Enables or disables any tutorial" above fill wid 2 ht .5
move down .05 from TUTEXPL.c
"menus before or during the game"

move down .25 from TUTEXPL.s
SAVEXPL: box "Enables or disables any high" above fill wid 2 ht .5
move down .05 from SAVEXPL.c
"score saving during the game"

move down .25 from SAVEXPL.s
VOLEXPL: box "Enables or disables any sound" above fill wid 2 ht .5
move down .05 from VOLEXPL.c
"effects during the game"

move down .25 from VOLEXPL.s
BACKEXPL: box "Sends user back to previous menu" fill wid 2 ht .25


arrow from TUTEXPL.w to TUTC.e
arrow from SAVEXPL.w to SAVC.e
arrow from VOLEXPL.w to VOLC.e
arrow from BACKEXPL.w to BACK.e

.PE
.KE

.KS
.NH 2
About Menu
.PP
.PS
OL: box wid 5 ht 5
move down 1 from OL.n
box fill "About:"
move down .5
AB: box  wid 3.5 ht 2
"This will contain all the information about the game," at AB.c
move down .15
"the authors and licensing"
move down .1 from AB.s
box "Back" fill ht .25
.PE
.KE



.KS
.NH 2
In game UI
.PP
.PS
OL: box "Game here lol" wid 5 ht 5
down
line .33 invis from OL.ne
left
line 0.65 invis
down
HLTH: box wid .15 ht 1
right
line .33 invis from HLTH.ne
down
GAS: box wid .15 ht 1
move down .1 from HLTH.s
box "Health" ht .13 wid .34
move down .1 from GAS.s
box "Gas %" ht .13 wid .34

move left .45 from OL.ne
move down .1
IO: box wid .75 ht 1.55
move down .1 from IO.n
"Floor: X"

move down .01 from IO.s
IO2: box wid .75 ht 1.55
move down .15 from IO.s
"WEP-NAME:"
move down .15
WEP: box wid .15 ht 1


move down 1 from OL.ne
move right .2
EXP: box "Non-Interactable TUI/GUI with" above fill wid 2 ht .5
move down .05 from EXP.c
"game stats"

move down .25 from EXP.s
WEPEXP: box "Weapon Health" fill wid 2 ht .25

arrow from EXP.w to IO.e
arrow from WEPEXP.w to WEP.e

.PE
The game will take up as much of the screen as possible, potentially even the whole screen with a window
above it that contains the player's health and gas%.
Other than the health and gas% bars (and potentially a compass), there will be no other UI elements, as 
the game should be as uncluttered as possible.

This is not to scale, the UI will still be in the top right, but will not scale to be larger on larger
terminal sizes so as to not clutter the screen up with garbage.
.KE

.KS
.NH 2
High scores screen
.PP
.PS
OL: box wid 5 ht 5
move up 1.8 from OL.c
TL: box "You died!" above fill ht .5 wid 2
move down .35
"High scores:"
move left .5 from TL.s
move down .3
S1: box "Score 1: " ht .3 wid 1
S2: box "Score 2: " ht .3 wid 1 from S1.s
S3: box "Score 3: " ht .3 wid 1 from S2.s
S4: box "Score 4: " ht .3 wid 1 from S3.s
S5: box "Score 5: " ht .3 wid 1 from S4.s
S6: box "Score 6: " ht .3 wid 1 from S5.s
S7: box "Score 7: " ht .3 wid 1 from S6.s
S8: box "Score 8: " ht .3 wid 1 from S7.s
S9: box "Score 9: " ht .3 wid 1 from S8.s
S10: box "Score 10: " ht .3 wid 1 from S9.s
move right 0.00 from S1.e
Sc1: box "Value1 " ht .3 wid 1
move down 0.00 from Sc1.s
Sc2: box "Value2 " ht .3 wid 1
Sc3: box "Value3 " ht .3 wid 1
Sc4: box "Value4 " ht .3 wid 1
Sc5: box "Value5 " ht .3 wid 1
Sc6: box "Value6 " ht .3 wid 1
Sc7: box "Value7 " ht .3 wid 1
Sc8: box "Value8 " ht .3 wid 1
Sc9: box "Value9 " ht .3 wid 1
Sc10: box "Value10 " ht .3 wid 1
move down .15 from S10.se
CONT: box "Press <Space> to continue" invis wid 1.75

move down 2 from OL.ne
move right .1
SCEXP: box fill ht .7 wid 2.25
move up .25 from SCEXP.c
"Score X will be replaced by a users"
move down .2
"name, values will be dynamically filled"
move down .2
"in with scores"
move down .25 from SCEXP.s
CONTEXP: box "Will continue to the game over screen" fill ht .25 wid 2.25

arrow from SCEXP.w to Sc1.e
arrow from CONTEXP.w to CONT.e
.PE
.PP
If the user achieves a highscore, an additional prompt will pop up.
.PS
OL: box ht 3 wid 5 fill
move up 1 from OL.c
"You got a high-score!"
move down .25
"Enter your name:"
move down .5
IN: box "|" ht .25 wid 2
move down .5 from IN.s
"Press <Enter> to continue"

move down 1.5 from OL.ne
move right .15
INEXP: box "Input for the user's name, will be" above fill ht .5 wid 2
move down .1 from INEXP.c
"limited to 3 chars."

arrow from INEXP.w to IN.e
.PE
.KE

.KS
.NH 2
Game over screen
.PP
.PS
OL: box wid 5 ht 5
move up 1 from OL.c
box "GAME OVER" fill wid 2 ht .5
move down .1 from OL.c
RS: box "Restart" wid 1 ht .25
move down .15 from RS.s
QU: box "Quit" wid 1 ht .25

move down 2 from OL.ne
move right .15
RSEXP: box "Restarts the game back to the start." fill wid 2 ht .25
move down .15 from RSEXP.s
QUEXP: box "Quits the game back to the menu." fill wid 2 ht .25

arrow from RSEXP.w to RS.e
arrow from QUEXP.w to QU.e
.PE
.KE

.KS
.NH 2
Typical game appearance
.PP
.PS
OL: box wid 5 ht 5
move right .5 from OL.nw
down
line .2
move .1
line .2
left
line .5
move down .25
right
line .7
move .1
line .2
down
line .6
left
line 1

move right .75 from OL.nw
down
line .2
move .1
line .2
right
line 1
move .1
line 1.5
up
line .5
right
move .25
down
line 2
move .1
line 1.9
right
line 1.4

move down 1.6 from OL.nw
right
RT: line 1.45
move .1
RT2: line 1.45
down
line 1.45
move .1
line 1.6
left
BL: line 3

move right .5 from RT.w
down
line .5
left
line .2
move .1
line .2

move right 1 from RT.w
down
line .5
left
line .2
move .1
line .2

move left .5 from RT2.e
down
line .5
right
line .2
move .1
line .2

move left 1 from RT2.e
down
line .5
right
line .2
move .1
line .2

move right .5 from BL.w
up
line .5
left
line .2
move .1
line .2

move right 1 from BL.w
up
line .5
left
line .2
move .1
line .2

move right 1.5 from BL.w
up
line .5
left
line .2
move .1
line .2

move right 2 from BL.w
up
line .5
left
line .2
move .1
line .2

move right 2.5 from BL.w
up
line .5
left
line .2
move .1
line .2

move right 3 from BL.w
up
line .5
left
line .2
move .1
line .2

move down .75 from RT.w
right
line 1.5
move .1
line .9
down
line .755
move .1
line .755
left
line .9
move .1
line 1.5

move right .15 from OL.c
"@"
move right 1
"V"
move down .75
"V"
move right .25
move up .4
"V"
move left 2.5
"V"
move up .67
move left .75
"&"
move down 2
move right 2
"&"

move up 3
"X"
move right 1
"X"
move up .75
move left .2
"X"

move down 1 from OL.ne
move right .2
KEY: box fill wid 1 ht 1
move down .2 from KEY.n
"KEY:"
move down .15
"@ = Player"
move down .15
"X = Enemy"
move down .15
"V = Potion"
move down .15
"& = Skill Scroll"


.PE
The level generation needs to be fairly simple, yet vary enough to change the feeling of the game.
Rooms not yet explored will not be visable until entered at least once, therefore players should
get some other kind of indication as to what is in a room. (Potentially a radar or compass as an
unlock) (The above is a really bad diagram)
.KE

.KS
.NH
Programme design
.PP
The game is obviously going to be written in C, as a mostly functional program with few 
Object-Oriented design decisions.
The program will be divided into a bunch of smaller 
.CW *.c
and 
.CW *.h
files, which in turn are compiled to
.CW *.o
files, which are then statically linked together into the game executable.
.B
FINAL LAYOUT TBD

.UL "Diagram of compilation process:"
.R
.PS
down
MK: box "make"
arrow .3 from MK.s
CC: box "cc"

move down .3 from CC.s
move left 2.5
down
MENh: box "menu.h"
move right .625 from MENh
GAMh: box "game.h"
move right .625 from GAMh
LEVh: box "level.h"
move right .625 from LEVh
ITMh: box "items.h"
move right .625 from ITMh
SCRh: box "score.h"
move right .625 from SCRh
ENMh: box "enemies.h"

arrow from CC.s to MENh.n
arrow from CC.s to GAMh.n
arrow from CC.s to LEVh.n
arrow from CC.s to ITMh.n
arrow from CC.s to SCRh.n
arrow from CC.s to ENMh.n

move down .3 from MENh.s
MENc: box "menu.c"
move right .625 from MENc
GAMc: box "game.c"
move right .625 from GAMc
LEVc: box "level.c"
move right .625 from LEVc
ITMc: box "item.c"
move right .625 from ITMc
SCRc: box "score.c"
move right .625 from SCRc
ENMc: box "enemies.c"

arrow from MENh.s to MENc.n
arrow from GAMh.s to GAMc.n
arrow from LEVh.s to LEVc.n
arrow from ITMh.s to ITMc.n
arrow from SCRh.s to SCRc.n
arrow from ENMh.s to ENMc.n

move down 1.9 from CC.s
LNK: box "link"
arrow .3
box "JigokuNoTou" wid 1

arrow from MENc.s to LNK.n
arrow from GAMc.s to LNK.n
arrow from LEVc.s to LNK.n
arrow from ITMc.s to LNK.n
arrow from SCRc.s to LNK.n
arrow from ENMc.s to LNK.n
.PE
List of files and their functions:
.TS
 _ _
|c|c|
 = =
|c|r|
 _ _
|c|r|
 _ _
|c|r|
 _ _
|c|r|
 _ _
|c|r|
 _ _
|c|r|
 _ _
|c|r|
 _ _
|c|r|
 _ _
|c|r|
 _ _
|c|r|
 _ _
|c|r|
 _ _
|c|r|
 _ _.

File:	Function:

menu.c	To draw the menu, and link to other files

menu.h	To define all the functions in use by menu.c

game.c	To bring all the game functions together, and draw them on the screen

game.h	To define all the functions in use be game.c

level.c	To generate a random level for the game

level.h	To define all the functions in use by level.c

items.c	To generate the items in a level, will know the positions of the walls and generate random items.

items.h	To define all the functions and variables in use by items.c

enemies.c	To generate and update enemy positions (Likely won't be very advanced)

enemies.h	To define all the functions in use by enemies.c

score.c	To keep track of (and maybe store the score)

score.h	To define all the functions in use by score.c

.TE
.KE

.NH 2
.CW menu.c 
functions:
.R

.P1
void aboutMenu();
.P2
This function is to draw the about menu, will clear the screen and print information about the
game, author etc. No inputs and no outputs.

.P1
WINDOW *button(int ht, int wid, int y, int x, char[] message, bool sel);
.P2
This window pointer function creates a button with the following attributes:
A position of 
.CW x , (
.CW y ) 
with width
.CW wid
and height
.CW ht .
Additionally takes an array of characters that make the displayed
.CW message ,
and the boolean 
.CW sel
to determine whether or not it should be drawn as the selected button.
All other button management goes on outside of this function, this simply gives properties
to the button.

.P1
void draw();
.P2
This is the function that handles the initialisation of colour, the default curser position and the
variable for quitting. Returns to the function where it is called from (Typically should be the
main function so that the program exits properly.

.P1
void navigation(bool quit);
.P2
This is the function that the menu will be in for the majority of the time, the
.CW quit boolean
given to it is also the function it will return true when quit.
Stays in a centre while loop for the most part, does not refresh every frame, opting
to wait for user input, as it is a menu.
Relies upon the
.CW title 
function, the
.CW *button 
function, and the
.CW selectButton
function in its typical operation.

.P1
bool selectButton(int active);
.P2
This is the function that determines what each button does when selected, takes the integer
.CW active
as the input, putting it into a case statement that determines what is run for each button.
As currently set up, an input of
.CW 0
will send the program to the game() function in 
.CW game.c .
An input of
.CW 1
will send the program to the settingsMenu() function, to set up the settings.
An input of
.CW 2
will send the program to the aboutMenu() function, to show the about page.
An input of
.CW 3
will return
.CW true ,
corresponding with the quit button, useful for exiting back out of a while loop.
Any other input will do nothing, corresponding to an undocumented button.

.P1
void settingsMenu();
.P2
Draws a menu to turn on or off high score saving, and configure audio (terminal beeping).
Should essentially act as another menu loop, with buttons to select in a similar way to the main
menu.

.P1
void title(int y, int x);
.P2
Draws the title described in
.CW menu.h 
by
the 
.CW titleA 
char
array at position
.CW x , (
.CW y ).

.NH 2
.CW game.c
functions:

.P1
void game();
.P2
The
.CW game()
function will be the main function that controls the game, should draw a frame on
every action.

.P1
void gameSpeedUp(int amount);
.P2
The
.CW gameSpeedUp()
function will take an integer and increase the game speed by that amount as a percentage of the
current speed.

.P1
void gameSpeedDown(int amount);
.P2
The
.CW gameSpeedDown()
function will take an integer and decrease the game speed by that amount as a percentage of the
current speed.

.P1
void playSpeedUp(int amount);
.P2
The
.CW playSpeedUp()
function will take an integer and increase the player's speed by that amount as a percentage of their
current speed.

.P1
void playSpeedDown(int amount);
.P2
The
.CW playSpeedDown()
function will take an integer and decrease the player's speed by that amount as a percentage of their
current speed.

.P1
void gasSpeedUp(int amount);
.P2
The
.CW gasSpeedUp()
function will take an integer and increase the speed of the gas by that amount as a percentage of its
current speed.

.P1
void gasSpeedDown(int amount);
.P2
The
.CW gasSpeedDown()
function will take an integer and decrease the speed of the gas by that amount as a percentage of its
current speed.

.P1
void gasValueUp(int amount);
.P2
The
.CW gasValueUp()
function will take an integer and increase the player's current gas % amount by that much as a percentage.

.P1
void gasValueDown(int amount);
.P2
The
.CW gasValueDown()
function will take an integer and decrease the player's current gas % amount by that much as a percentage.

.P1
void healthValueUp(int amount);
.P2
The
.CW healthValueUp()
function will take an integer and increase the player's current health percentage by the given amount.

.P1
void healthValueDown(int amount);
.P2
The
.CW healthValueDown()
function will take an integer and decrease the player's current health percentage by the given amount.

.P1
void gotoLevel(int floor, long int seed);
.P2
The
.CW gotoLevel()
function will take an integer for the floor and the seed to use, and will run all the required functions
to go to the given level.

.P1
void addEffect(int item);
.P2
The
.CW addEffect()
function will take an integer representing the item, and apply the necessary effects to the player using
the effect functions in
.CW game.c .

.NH 2
.CW level.c
functions:

.P1
int seedGen();
.P2
The
.CW seedGen()
function will generate and return a seed every time it is called.

.P1
char *level(int floor, long int seed);
.P2
The
.CW level()
function will generate the level based on the input seed.
Will return the level to wherever it is called from. Input
.CW floor
is to ensure that each level is different despite having the same global seed, allowing one to technically
descend a level (potential power up or gameplay aspect.)


.NH 2
.CW items.c
functions:

.P1
int *items(int floor, long int seed);
.P2
The 
.CW items()
function will generate and return random items that are in positions that don't clash with the wall generating
algorithm in
.CW level() .
The items will be described by preset integer values to be converted later for use with drawing and item effects.

.P1
char getItem(char *items, int y, int x);
.P2
The
.CW getItem()
function will take the input array describing the current field of 
.CW items ,
and position
.CW x , (
.CW y ),
and will ouput a character describing the item to be drawn on screen.
Should be a fast function, as it will be called on a regular basis every time a item needs to be drawn.

.NH 2
.CW score.c
functions:

.P1
int scoreMultiplier(int level);
.P2
The
.CW scoreMultiplier()
function will take the level that the player is on, and spit out the appropriate multiplier for that level.

.P1
void putHighScore(char[] name, int score);
.P2
.P1
void getHighScores();
.P2
These two functions will be responsible for the saving of high scores. The
.CW putHighScore()
function will take a name, and a score and append it to a new line of the high scores file, which will be located
in the directory that the game is runinng in.

.P1
void sortScores();
.P2
A function that sorts the high scores in the scores file by their size, so that printing the contents of the file
will result in a sorted list without any further modifications.

.NH 2
.CW enemies.c
functions:

.P1
char *placeEnemies(int floor, long int seed);
.P2
This function will set the initial position for the enemies, placing them in around the level, it will be different
depending on the seed used.

.P1
void *moveEnemies(int enemySpeed, int playerY, int playerX, char *enemyPositions, char *level);
.P2
This function will move the enemies closer to the player, by using the player's (x,y) position, and very VERY basic
pathfinding (I.E. Head straight for them once seen)

.P1
boolean canSee(char *level, int enemyY, int enemyX, int playerY, int playerX);
.P2
This boolean will return true if an enemy is in eyesight of the player, essentially making the enemy able to move.

